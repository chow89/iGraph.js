function iGraph(container, width, height) {

    var canvas = document.createElement("canvas"); // Create canvas element

    var gc = canvas.getContext("2d");
    var viewport = new Viewport(canvas);
    var grid = new Grid();
    var lens = new Lens();
    var radar = new Radar();
    var graph = new RandomGraph(0, 0);

    var lock; // Object locking during drag operations
    var requestID = 0; // The requested animation frame's id

    var requestUpdate = function () {
        if (requestID == 0) {
            requestID = window.requestAnimationFrame(update);
        }
    };

    var update = function (time) {
        var needUpdate = false;
        needUpdate = viewport.update(time) || needUpdate;
        needUpdate = lens.update(time) || needUpdate;
        needUpdate = radar.update(time) || needUpdate;

        // Prepare array of positions to be transformed with lens and radar
        var nTransformedPosition = [];
        var n = graph.nodes.length;
        while (n--) {
            nTransformedPosition[n] = graph.nLayout[n].slice();
        }

        // Apply transformations
        lens.transform(graph, viewport, nTransformedPosition);
        radar.transform(graph, viewport, nTransformedPosition);

        // Update positions according to lens and radar transformations
        n = graph.nodes.length;
        while (n--) {
            graph.nPosition[n].set(nTransformedPosition[n]);
        }

        // Update and project updated graph
        needUpdate = graph.update(time) || needUpdate;
        graph.project(viewport); // Project graph

        // Draw graph
        gc.clearRect(0, 0, canvas.width, canvas.height); // Clear the canvas
        grid.draw(gc, viewport); // Draw the grid
        graph.drawEdges(gc); // Draw all edges
        lens.localEdge(gc, viewport, grid, graph); // Replace lens interior with local edges
        graph.drawNodes(gc, lens.active, radar.active); // Draw all nodes
        lens.draw(gc, viewport); // Draw the lens
        radar.draw(gc); // Draw the radar

        // Request next animation frame if needed
        requestID = needUpdate ? window.requestAnimationFrame(update) : 0;
    };

    var reset = function () {
        viewport.fitToView([-Constants.LAYOUT_EXTENT / 2, -Constants.LAYOUT_EXTENT / 2, Constants.LAYOUT_EXTENT, Constants.LAYOUT_EXTENT], false, true);
        lens.position.set([0, 0]);
        lens.radius.set([0.2 * Constants.LAYOUT_EXTENT]);
        radar.position.set([canvas.width - Constants.RADAR_RADIUS - 15, canvas.height - Constants.RADAR_RADIUS - 15]);
        radar.radius.set([Constants.RADAR_RADIUS]);
        radar.range.set([Constants.RADAR_RANGE]);
        requestUpdate();
    };

    var handle = function (handler, evt) {
        // handler is the method name of the handle (e.g., "onclick", "onmousemove", or "onwheel")
        // evt is the corresponding event object

        // console.log(evt);
        var clientRect = canvas.getBoundingClientRect();
        evt.screenCoords = [evt.clientX - clientRect.left, evt.clientY - clientRect.top];
        evt.worldCoords = viewport.unproject(evt.screenCoords);

        // If a dragging operation is going on remain locked on that operation
        if (lock && lock[handler] && lock[handler](evt)) {
            requestUpdate();
            return lock;
        }

        // The trick is as follows:
        // xyz[handler] checks if a handler method exists for xyz
        // xyz.pick(evt) checks if xyz is under pointer
        // xyz[handler](evt) actually calls the handler method, which returns true if the evt has been consumed

        // These tests are done in a cascade, which continues until evt has been consumed:
        // 1. radar
        // 2. lens
        // 3. graph
        // else viewport

        var handledBy; // Store the handler that consumed the event
        if (radar[handler] && radar.pick(evt) && radar[handler](evt)) {
            handledBy = radar;
        }
        else if (lens[handler] && lens.pick(evt) && lens[handler](evt)) {
            handledBy = lens;
        }
        else if (graph[handler] && graph.pick(evt) && graph[handler](evt)) {
            handledBy = graph;
        }
        else {
            viewport[handler] && viewport[handler](evt);
            handledBy = viewport;
        }

        requestUpdate();
        return handledBy;
    };

    var onmousedown = function (evt) {
        if (evt.target == canvas) {
            evt.preventDefault();
            if (!lock) lock = handle("onmousedown", evt); // Lock onto object that handled mousedown
        }
    };

    var onmouseup = function (evt) {
        if (evt.target == canvas || lock) {
            evt.preventDefault();
            if (lock == handle("onmouseup", evt)) lock = undefined; // Release lock on object
        }
    };

    var onmousemove = function (evt) {
        if (evt.target == canvas || lock) {
            evt.preventDefault();
            handle("onmousemove", evt);
        }
    };

    var onclick = function (evt) {
        evt.preventDefault();
        handle("onclick", evt);
    };

    var ondblclick = function (evt) {
        evt.preventDefault();
        handle("ondblclick", evt);
    };

    var onwheel = function (evt) {
        evt.preventDefault();
        handle("onwheel", evt);
    };

    var oncontextmenu = function (evt) {
        evt.preventDefault();
    };

    var onkeypress = function (evt) {
        switch (evt.charCode) {
            case Constants.KEY_PLUS:
                viewport.scale(Constants.ZOOM_KEY_FACTOR);
                requestUpdate();
                break;
            case Constants.KEY_MINUS:
                viewport.scale(1 / Constants.ZOOM_KEY_FACTOR);
                requestUpdate();
                break;
            case Constants.KEY_W:
                viewport.scroll(0, -Constants.MOVE_KEY_FACTOR);
                requestUpdate();
                break;
            case Constants.KEY_S:
                viewport.scroll(0, Constants.MOVE_KEY_FACTOR);
                requestUpdate();
                break;
            case Constants.KEY_D:
                viewport.scroll(Constants.MOVE_KEY_FACTOR, 0);
                requestUpdate();
                break;
            case Constants.KEY_A:
                viewport.scroll(-Constants.MOVE_KEY_FACTOR, 0);
                requestUpdate();
                break;
            case Constants.KEY_G:
                grid.active = ! grid.active;
                requestUpdate();
                break;
            case Constants.KEY_ONE:
                graph.doSize = ! graph.doSize;
                graph.encodeSizeAndColor();
                requestUpdate();
                break;
            case Constants.KEY_TWO:
                graph.doColor = ! graph.doColor;
                graph.encodeSizeAndColor();
                requestUpdate();
                break;
            case Constants.KEY_THREE:
                graph.doWidth = ! graph.doWidth;
                graph.encodeSizeAndColor();
                requestUpdate();
                break;
            case Constants.KEY_SPACE:
                graph.layout.jiggle(Constants.LAYOUT_EXTENT);
                requestUpdate();
                break;
        }
    };

    var onresize = function () {
        var wasEnlarged = (canvas.width < window.innerWidth || canvas.height < window.innerHeight);
        canvas.width = window.innerWidth;
        canvas.height = window.innerHeight;
        viewport.fitToView(viewport.getDestination().slice(), false, wasEnlarged);
        radar.position.set([canvas.width - radar.radius.get()[0] - 15, canvas.height - radar.radius.get()[0] - 15]);
        requestUpdate();
    };

    canvas.addEventListener("click", onclick);
    canvas.addEventListener("dblclick", ondblclick);
    canvas.addEventListener("wheel", onwheel);
    canvas.addEventListener("contextmenu", oncontextmenu);

    window.addEventListener("mousedown", onmousedown);
    window.addEventListener("mouseup", onmouseup);
    window.addEventListener("mousemove", onmousemove);
    window.addEventListener("keypress", onkeypress);

    var $btn = { // The buttons to control iGraph
        miserables: $('<span class="button-xsmall pure-button"><i class="fa fa-folder-open-o"></i> Les Miserables Graph</span>'),
        random: $('<span class="button-small pure-button"><i class="fa fa-cogs"></i> Random Graph</span>'),
        radar: $('<span class="button-small pure-button"><i class="fa fa-wifi fa-rotate-90"></i> Radar</span>'),
        lens: $('<span class="button-small pure-button"><i class="fa fa-search"></i> Lens</span>'),
        localedges: $('<span class="button-small pure-button group first"><i class="fa fa-angle-double-right"></i> Local Edges</span>'),
        bringneighbors: $('<span class="button-small pure-button group"><i class="fa fa-angle-double-right"></i> Bring Neighbors</span>'),
        fisheye: $(' <span class="button-small pure-button group last"><i class="fa fa-angle-double-right"></i> Fisheye Distortion</span>'),
        reset: $('<span class="button-small pure-button"><i class="fa fa-crosshairs fa-lg"></i></span>'),
        zoomin: $('<span class="button-small pure-button"><i class="fa fa-search-plus fa-lg"></i></span>'),
        zoomout: $('<span class="button-small pure-button"><i class="fa fa-search-minus fa-lg"></i></span>')
    };

    // Helper functions for quick enabling and activating of buttons
    var btnEnable = function (enabled, btn) {
        var i = btn.length;
        while (i--) {
            if (enabled) {
                btn[i].prop("disabled", false);
                btn[i].removeClass("pure-button-disabled");
            }
            else {
                btn[i].prop("disabled", true);
                btn[i].addClass("pure-button-disabled");

            }
        }
    };

    var btnActivate = function (activated, btn) {
        var i = btn.length;
        while (i--) {
            if (activated) {
                btn[i].addClass("pure-button-active");
            }
            else {
                btn[i].removeClass("pure-button-active");
            }
        }
    };

    // Attach click handlers to buttons

    $btn.miserables.click(
        function () {
            graph = new JSONGraph(json);
            reset();
        });

    $btn.random.click(
        function () {
            graph = new RandomGraph(Constants.NUMBER_NODES, Constants.NUMBER_EDGES);
            reset();
        });

    $btn.radar.click(
        function () {
            radar.active = !radar.active;
            btnActivate(radar.active, [$btn.radar]);
            requestUpdate();
        });

    $btn.lens.click(
        function () {
            if (!lens.hasBeenUsedBefore) { // First time use of the lens
                lens.hasBeenUsedBefore = true;
                var vp = viewport.get();
                console.log(vp);
                lens.position.set([vp[0] + vp[2] / 2, vp[1] + vp[3] / 2]); // Center on canvas
                lens.radius.set([0.2 * Math.min(vp[2], vp[3])]); // Make reasonable size
            }
            lens.active = !lens.active;
            btnActivate(lens.active, [$btn.lens]);
            btnEnable(lens.active, [$btn.localedges, $btn.bringneighbors, $btn.fisheye]);
            requestUpdate();
        });

    $btn.localedges.click(
        function () {
            if (!lens.active) return;
            lens.doLocalEdges = !lens.doLocalEdges;
            btnActivate(lens.doLocalEdges, [$btn.localedges]);
            requestUpdate();
        });

    $btn.bringneighbors.click(
        function () {
            if (!lens.active) return;
            lens.doBringNeighbors = !lens.doBringNeighbors;
            btnActivate(lens.doBringNeighbors, [$btn.bringneighbors]);
            requestUpdate();
        });

    $btn.fisheye.click(
        function () {
            if (!lens.active) return;
            lens.doFisheye = !lens.doFisheye;
            btnActivate(lens.doFisheye, [$btn.fisheye]);
            requestUpdate();
        });

    $btn.reset.click(
        function () {
            reset();
        });

    $btn.zoomin.click(
        function () {
            viewport.scale(Constants.ZOOM_WHEEL_FACTOR);
            requestUpdate();
        });

    $btn.zoomout.click(
        function () {
            viewport.scale(1 / Constants.ZOOM_WHEEL_FACTOR);
            requestUpdate();
        });

    var $menu = $('<div class="igraph-buttons">').append([$btn.miserables, $btn.random, $btn.radar, $btn.lens, $btn.localedges, $btn.bringneighbors, $btn.fisheye, $('<br>'), $btn.reset, $btn.zoomin, $btn.zoomout]);
    $(container).append($menu);
    $(container).append(canvas);

    if (width == "fullscreen") {
        $(document.body).addClass("igraph-fullscreen");
        $(container).addClass("igraph-fullscreen");
        $(canvas).addClass("igraph-fullscreen");
        $menu.addClass("igraph-fullscreen");
        $.each($btn, function (key, value) {
            value.addClass("igraph-fullscreen");
        });
        window.addEventListener("resize", onresize);
        canvas.height = window.innerHeight;
        canvas.width = window.innerWidth;
    }
    else {
        canvas.width = width;
        canvas.height = height;
    }

    // Set defaults
    btnActivate(true, [$btn.localedges]);
    btnEnable(false, [$btn.miserables, $btn.localedges, $btn.bringneighbors, $btn.fisheye]);

    // Load graph from JSON file
    var json;
    var req = new XMLHttpRequest();
    req.open("GET", "lesmiserables.json");
    req.onload = function () {
        btnEnable(true, [$btn.miserables]);

        // Immediately load the lesmiserables graph
        json = JSON.parse(this.responseText);
        graph = new JSONGraph(json);
        reset();
    };
    req.onerror = function (e) {
        console.log(e);
    };
    req.send();

    // Start rendering
    reset();
}