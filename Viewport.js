function Viewport(canvas) {
    Animatable.call(this, [0, 0, canvas.width, canvas.height]);
    this.canvas = canvas;
}

Viewport.prototype = Object.create(Animatable.prototype);

Viewport.prototype.project = function (p) { // Project world coordinates to view coordinates
    if (p.length == 1) return [p[0] / this.cur[2] * this.canvas.width];
    return [(p[0] - this.cur[0]) / this.cur[2] * this.canvas.width, (p[1] - this.cur[1]) / this.cur[3] * this.canvas.height];
};

Viewport.prototype.unproject = function (p) { // Unproject view coordinates to world coordinates
    if (p.length == 1) return [this.cur[2] * p[0] / this.canvas.width];
    return [this.cur[0] + this.cur[2] * p[0] / this.canvas.width, this.cur[1] + this.cur[3] * p[1] / this.canvas.height];
};

Viewport.prototype.scale = function (s, cx, cy) { // Scale by s at center cx,cy
    cx = cx || 0.5;
    cy = cy || 0.5;

    this.set([this.cur[0] + this.cur[2] * cx * (1 - s), this.cur[1] + this.cur[3] * cy * (1 - s), this.cur[2] * s, this.cur[3] * s]);
};

Viewport.prototype.translate = function (dx, dy) { // Translation
    this.set([this.cur[0] + dx, this.cur[1] + dy, this.cur[2], this.cur[3]]);
};

Viewport.prototype.scroll = function (wx, wy) { // Scroll (1 means scroll one viewport extent in the desired direction)
    this.set([this.cur[0] + wx * this.cur[2], this.cur[1] + wy * this.cur[3], this.cur[2], this.cur[3]]);
};
Viewport.prototype.center = function (cx, cy) { // Center at cx, cy
    this.set([cx - this.cur[2] * 0.5, cy - this.cur[3] * 0.5, this.cur[2], this.cur[3]]);
};

Viewport.prototype.fitToView = function (worldBounds, inflate, scaleORcut) {
    var viewWidth = Math.max(this.canvas.width, 1);
    var viewHeight = Math.max(this.canvas.height, 1);
    var viewAspect = viewWidth / viewHeight;

    var worldAspect = worldBounds[2] / worldBounds[3];

    var r = worldBounds.slice();

    // true: whole content in worldBounds will be visible
    // false: only part of worldBounds will be visible
    if (scaleORcut) {
        if (viewAspect > worldAspect) {
            r[2] = r[3] * viewAspect; // Enlarge viewport to make view fit to screen; critical: if panel is very small viewport must be very large to fit view to screen
        }
        else {
            r[3] = r[2] / viewAspect; // Enlarge viewport to make view fit to screen; critical: if panel is very small viewport must be very large to fit view to screen
        }
    }
    else {
        if (viewAspect > worldAspect) {
            r[3] = r[2] / viewAspect; // Reduce viewport; view will be centered
        }
        else {
            r[2] = r[3] * viewAspect; // Reduce viewport; view will be centered
        }
    }

    if (inflate) {
        r[2] *= 1.2;
        r[3] *= 1.2;
    }

    r[0] += (worldBounds[2] - r[2]) / 2;
    r[1] += (worldBounds[3] - r[3]) / 2;

    this.set(r);
};

Viewport.prototype.onmousedown = function (evt) {
    if (evt.button == 2) {
        this.dragging = {
            start: evt.screenCoords, // Coordinates where drag started
            p: this.cur.slice(0, 2) // Position of viewport when drag started
        };
        return true; // Event consumed
    }
    // Event not consumed
};

Viewport.prototype.onmouseup = function (evt) {
    if (this.dragging) {
        if (evt.button == 2) {
            delete this.dragging;
            return true; // Event consumed
        }
    }
    // Event not consumed
};

Viewport.prototype.onmousemove = function (evt) {
    if (this.dragging) {
        // Important: Unproject single value only! Otherwise, unporject takes position
        // of viewport into account, but this position is currently undergoing a drag operation. Also, the
        // drag operation must be computed based on screen coordinates, because the world coordinates
        // reported in evt.worldCoords are already based on the currently dragged and changed viewport.
        var dx = this.unproject([this.dragging.start[0] - evt.screenCoords[0]])[0];
        var dy = this.unproject([this.dragging.start[1] - evt.screenCoords[1]])[0];
        this.set([this.dragging.p[0] + dx, this.dragging.p[1] + dy, this.cur[2], this.cur[3]]);
        return true; // Event consumed
    }
    // Event not consumed
};

Viewport.prototype.ondblclick = function (evt) {
    if (evt.button == 0) {
        // Center viewport at mouse coordinates
        this.center(evt.worldCoords[0], evt.worldCoords[1]);
        return true; // Event consumed
    }
    // Event not consumed
};

Viewport.prototype.onwheel = function (evt) {
    if (!evt.shiftKey) {
        var delta = evt.deltaY ? evt.deltaY : evt.deltaX;
        var cx, cy;
        // Zoom on mouse cursor
        cx = evt.screenCoords[0] / this.canvas.width;
        cy = evt.screenCoords[1] / this.canvas.height;
        // Zoom on center of screen
        // cx = 0.5;
        // cy = 0.5;
        this.scale(delta < 0 ? Constants.ZOOM_WHEEL_FACTOR : 1 / Constants.ZOOM_WHEEL_FACTOR, cx, cy);
        return true; // Event consumed
    }
    // Event not consumed
};
