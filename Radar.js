function Radar() {

    this.active = false;

    this.position = new Animatable([0, 0]);
    this.radius = new Animatable([Constants.RADAR_RADIUS]);
    this.angle = new Animatable([0]);
    this.range = new Animatable([Math.PI / 2]);

    this.nodesInRadar = [];

    this.picked = false;
}

Radar.prototype.pick = function (evt) {
    if (!this.active) return false;

    var pos = this.position.get();
    var rad = this.radius.get()[0];
    var a = this.angle.get()[0];
    var r = this.range.get()[0];

    // Test angle between pointer and radar

    var ux = Math.cos(a); // Vector in direction of radar
    var uy = Math.sin(a);
    var ul = 1;

    var vx = evt.screenCoords[0] - pos[0]; // Vector from center to point
    var vy = evt.screenCoords[1] - pos[1];
    var vl = Math.sqrt(vx * vx + vy * vy);

    this.picked = false;

    if (!vl) { // Pointer is directly at center of radar
        this.picked = "radar";
    }
    else { // Pointer is at a distance from center of radar
        var cos = (ux * vx + uy * vy) / (ul * vl); // Cosine between vectors
        var angle = Math.acos(cos); // Angle between vectors

        if (angle < r / 2) {
            if (vl < rad * Constants.RADAR_HANDLE_SCALE) { // The handle's radius is a bit extended
                this.picked = "handle";
            }
        }
        else {
            if (vl < rad) { // Regular radius
                this.picked = "radar";
            }
        }
    }

    return this.picked;
};

Radar.prototype.onmousedown = function (evt) {
    if (this.active && !this.dragging) {
        if (evt.button == 0) {
            // Determine whether to drag radar or its handle
            if (this.picked == "handle") { // If angle is in range
                // We will be dragging the handle
                this.dragging = {
                    p: evt.screenCoords.slice()
                };
            }
            else {
                // We will be dragging the radar
                this.dragging = {
                    start: evt.screenCoords.slice(), // Coordinates where drag started
                    position: this.position.get().slice(0, 2) // Position of radar when drag started
                };
            }
        }
    }
    return true; // Event consumed
};

Radar.prototype.onmouseup = function (evt) {
    if (this.dragging) {
        if (evt.button == 0) {
            delete this.dragging;
        }
    }
    return true; // Event consumed
};

Radar.prototype.onmousemove = function (evt) {
    if (this.dragging) {
        if (this.dragging.position) {
            var dx = evt.screenCoords[0] - this.dragging.start[0];
            var dy = evt.screenCoords[1] - this.dragging.start[1];
            this.position.set([this.dragging.position[0] + dx, this.dragging.position[1] + dy]);
        }
        else {
            var pos = this.position.get();
            var ux = this.dragging.p[0] - pos[0];
            var uy = this.dragging.p[1] - pos[1];
            var ul = Math.sqrt(ux * ux + uy * uy);

            var vx = evt.screenCoords[0] - pos[0];
            var vy = evt.screenCoords[1] - pos[1];
            var vl = Math.sqrt(vx * vx + vy * vy);

            if (ul && vl) {
                var sign = (ux * vy - uy * vx) < 0 ? -1 : 1;
                var cos = (ux * vx + uy * vy) / (ul * vl);
                var angle = Math.acos(cos < 1 ? cos : 1);

                this.angle.set([this.angle.getDestination()[0] + sign * angle]);
                this.dragging.p = evt.screenCoords.slice();
            }
        }
    }
    return true; // Event consumed
};

Radar.prototype.onwheel = function (evt) {
    if (this.active) {
        if (evt.shiftKey) {
            var delta = evt.deltaY ? evt.deltaY : evt.deltaX;
            if (this.picked == "handle") {
                var r = this.range.get()[0];
                r = delta > 0 ? r * Constants.ZOOM_WHEEL_FACTOR : r / Constants.ZOOM_WHEEL_FACTOR;
                r = Math.min(r, Math.PI * 2);
                this.range.set([r]);
            }
            else {
                var rad = this.radius.get()[0];
                rad = delta > 0 ? rad * Constants.ZOOM_WHEEL_FACTOR : rad / Constants.ZOOM_WHEEL_FACTOR;
                this.radius.set([rad]);
            }
        }
    }
    return true; // Event consumed
};

Radar.prototype.onclick = function (evt) {
    return true; // Event consumed
};

Radar.prototype.ondblclick = function (evt) {
    return true; // Event consumed
};

Radar.prototype.update = function (time) {
    // var a = this.angle.get()[0] + Math.PI / 10;
    // this.angle.set([a]);
    var needUpdate = false;
    needUpdate = this.position.update(time) || needUpdate;
    needUpdate = this.radius.update(time) || needUpdate;
    needUpdate = this.angle.update(time) || needUpdate;
    needUpdate = this.range.update(time) || needUpdate;
    return needUpdate;
};

Radar.prototype.draw = function (gc) {
    if (!this.active) return;

    var pos = this.position.get();
    var rad = this.radius.get()[0];
    var a = this.angle.get()[0];
    var r = this.range.get()[0];

    gc.save();

    // Canvas center and diagonal to canvas edge
    var cpos = [gc.canvas.width / 2, gc.canvas.height / 2];
    var cedge = Math.sqrt(cpos[0] * cpos[0] + cpos[1] * cpos[1]);

    gc.lineWidth = Constants.LINE_WIDTH_RADAR;
    gc.shadowColor = 'rgb(64, 64, 64)';
    gc.shadowOffsetX = 2;
    gc.shadowOffsetY = 2;
    gc.shadowBlur = 4;

    // Draw radar arc
    gc.beginPath();
    gc.moveTo(cpos[0], cpos[1]);
    gc.arc(cpos[0], cpos[1], cedge, a - r / 2, a + r / 2);
    gc.closePath();

    gc.fillStyle = Constants.COLOR_RADAR_ARC;
    gc.globalAlpha = 0.1;
    gc.fill();
    gc.globalAlpha = 1;
    gc.strokeStyle = Constants.COLOR_LINE_REGULAR;
    gc.stroke();

    // Draw radar wheel
    gc.beginPath();
    gc.arc(pos[0], pos[1], rad, a + r / 2, a - r / 2);
    gc.lineTo(pos[0], pos[1]);
    gc.closePath();

    gc.strokeStyle = Constants.COLOR_LINE_REGULAR;
    gc.stroke();

    // Draw radar arc handle
    gc.beginPath();
    gc.moveTo(pos[0], pos[1]);
    gc.arc(pos[0], pos[1], rad * Constants.RADAR_HANDLE_SCALE, a - r / 2, a + r / 2);
    gc.closePath();

    gc.fillStyle = Constants.COLOR_RADAR_ARC_HANDLE;
    gc.fill();
    gc.strokeStyle = this.picked == "handle" ? Constants.COLOR_LINE_HIGHLIGHT : Constants.COLOR_LINE_REGULAR;
    gc.shadowOffsetX = 0;
    gc.shadowOffsetY = 0;
    gc.shadowBlur = 0;
    gc.stroke();

    gc.restore();
};

Radar.prototype.transform = function (graph, viewport, position) {

    this.nodesInRadar = [];

    this.select(graph, viewport, position);
    this.projectToViewport(viewport, position);

};

Radar.prototype.select = function (graph, viewport, position) {
    if (!this.active) return;
    var a = this.angle.get()[0];
    var r = this.range.get()[0];
    var v = viewport.get();
    var c = [v[0] + v[2] / 2, v[1] + v[3] / 2];

    var ux = Math.cos(a); // Vector in direction of radar
    var uy = Math.sin(a);
    var ul = 1;

    var p;
    var vx, vy, vl, cos, angle;
    var n = graph.nodes.length;
    while (n--) {
        p = position[n];

        // Test position outside viewport
        if (!(p[0] >= v[0] && p[0] < v[0] + v[2] && p[1] >= v[1] && p[1] < v[1] + v[3])) { // Outside test == !(inside test)
            // For nodes that are outside
            // Test angle between node and radar
            vx = p[0] - c[0]; // Vector from center to node
            vy = p[1] - c[1];
            vl = Math.sqrt(vx * vx + vy * vy);

            if (!vl) continue;

            cos = (ux * vx + uy * vy) / (ul * vl); // Cosine between radar vector and node vector
            angle = Math.acos(cos); // Angle between radar vector and node vector

            graph.nInRadar[n] = (angle < r / 2); // Angle must be smaller than half of the radar range
        }
        else {
            graph.nInRadar[n] = false; // Node is inside viewport and therefore not affected by radar
        }

        if (graph.nInRadar[n]) this.nodesInRadar.push(n);
    }
};

Radar.prototype.projectToViewport = function (viewport, position) {
    if (!this.active) return;
    var v = viewport.get();
    var c = [v[0] + v[2] / 2, v[1] + v[3] / 2];

    var p;
    var n;
    var i = this.nodesInRadar.length;
    while (i--) {
        n = this.nodesInRadar[i];
        p = position[n];
        p = this.clip(v, p, c);

        position[n][0] = p[0];
        position[n][1] = p[1];
    }
};

Radar.prototype.clip = function (viewport, p, c) {

    // Clipping function adapted from:
    // Liang-Barsky function by Daniel White @ http://www.skytopia.com/project/articles/compsci/clipping.html
    //
    // Our variant assumes that c is guaranteed to be inside viewport. Hence, we only return the clipped p.
    // The empty return statements should actually never happen in our special case!

    var t0 = 0.0;
    var t1 = 1.0;
    var dx = c[0] - p[0];
    var dy = c[1] - p[1];
    var P, Q, R;

    for (var edge = 0; edge < 4; edge++) {   // Traverse through edges of viewport (minX, maxX, minY, maxY)
        if (edge == 0) {
            P = -dx;
            Q = -(viewport[0] - p[0]);
        }
        if (edge == 1) {
            P = dx;
            Q = (viewport[0] + viewport[2] - p[0]);
        }
        if (edge == 2) {
            P = -dy;
            Q = -(viewport[1] - p[1]);
        }
        if (edge == 3) {
            P = dy;
            Q = (viewport[1] + viewport[3] - p[1]);
        }
        R = Q / P;
        if (P == 0 && Q < 0) return; // Don't draw line at all. (parallel line outside)

        if (P < 0) {
            if (R > t1) return; // Don't draw line at all.
            else if (R > t0) t0 = R; // Line is clipped!
        } else if (P > 0) {
            if (R < t0) return; // Don't draw line at all.
            else if (R < t1) t1 = R; // Line is clipped!
        }
    }

    return [p[0] + t0 * dx, p[1] + t0 * dy]; // (clipped) line is drawn
};