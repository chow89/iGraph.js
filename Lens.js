function Lens() {

    this.active = false;
    this.doLocalEdges = true;
    this.doBringNeighbors = false;
    this.doFisheye = false;

    this.position = new Animatable([0, 0]);
    this.radius = new Animatable([0]);
    this.magnification = new Animatable([Constants.FISHEYE_MAGNIFICATION]);

    this.nodesInLens = [];

    this.lDrawPosition = 0;
    this.lDrawRadius = 0;

    this.picked = false;
    this.centerTolerance = 0;
}

Lens.prototype.pick = function (evt) {
    if (!this.active) return false;

    var c = this.position.get();
    var r = this.radius.get()[0];
    var dx = evt.worldCoords[0] - c[0];
    var dy = evt.worldCoords[1] - c[1];

    this.picked = (dx * dx + dy * dy < r * r);
    evt.target.style.cursor = this.picked && this.active ? 'pointer' : 'default';

    return this.picked;
};

Lens.prototype.onmousedown = function (evt) {
    if (evt.button == 0) {
        this.dragging = {
            start: evt.worldCoords, // Coordinates where drag started
            p: this.position.get().slice(0, 2) // Position of lens when drag started
        };
        return true; // Event consumed
    }
    // Event not consumed
};

Lens.prototype.onmouseup = function (evt) {
    if (this.dragging) {
        if (evt.button == 0) {
            delete this.dragging;
            return true; // Event consumed
        }
    }
    // Event not consumed
};

Lens.prototype.onmousemove = function (evt) {
    if (this.dragging) {
        var dx = evt.worldCoords[0] - this.dragging.start[0];
        var dy = evt.worldCoords[1] - this.dragging.start[1];
        this.position.set([this.dragging.p[0] + dx, this.dragging.p[1] + dy]);
        return true; // Event consumed
    }
    // Event not consumed
};

Lens.prototype.onwheel = function (evt) {
    if (evt.shiftKey) {
        var delta = evt.deltaY ? evt.deltaY : evt.deltaX;
        var r = this.radius.get()[0];
        this.radius.set([delta > 0 ? r * Constants.ZOOM_WHEEL_FACTOR : r / Constants.ZOOM_WHEEL_FACTOR]);
        return true; // Event consumed
    }
    // Event not consumed
};

Lens.prototype.update = function (time) {
    var needUpdate = false;
    needUpdate = this.position.update(time) || needUpdate;
    needUpdate = this.radius.update(time) || needUpdate;
    return needUpdate;
};

Lens.prototype.project = function (viewport) {
    this.lDrawPosition = viewport.project(this.position.get());
    this.lDrawRadius = viewport.project(this.radius.get())[0];
    this.centerTolerance = viewport.unproject([Constants.BRING_NEIGHBORS_CENTER_TOLERANCE])[0]; // Tolerance for interaction
};

Lens.prototype.draw = function (gc) {
    if (this.active) {
        gc.save();

        gc.strokeStyle = Constants.COLOR_LINE_REGULAR;
        gc.lineWidth = Constants.LINE_WIDTH_LENS;
        gc.shadowColor = 'rgb(64, 64, 64)';
        gc.shadowOffsetX = 2;
        gc.shadowOffsetY = 2;
        gc.shadowBlur = 4;

        gc.beginPath();
        gc.arc(this.lDrawPosition[0], this.lDrawPosition[1], this.lDrawRadius, 0, Math.PI * 2);
        gc.stroke();

        gc.beginPath();
        gc.arc(this.lDrawPosition[0], this.lDrawPosition[1], Constants.BRING_NEIGHBORS_CENTER_TOLERANCE / 2, 0, Math.PI * 2);
        gc.stroke();

        gc.restore();
    }
};

Lens.prototype.localEdge = function (gc, viewport, grid, graph) {
    if (this.active && this.doLocalEdges) { // Draw edge filter lens
        gc.save(); // Save gc state

        var selectedEdges = [];
        var e = graph.edges.length;
        while (e--) {
            graph.eInLens[e] = graph.nInLens[graph.edges[e].src] || graph.nInLens[graph.edges[e].dst];
            if (graph.eInLens[e]) selectedEdges.push(e);
        }

        // Set up clip and clear the lens
        gc.beginPath();
        gc.arc(this.lDrawPosition[0], this.lDrawPosition[1], this.lDrawRadius, 0, Math.PI * 2);
        gc.clip();
        gc.clearRect(this.lDrawPosition[0] - this.lDrawRadius, this.lDrawPosition[1] - this.lDrawRadius, 2 * this.lDrawRadius, 2 * this.lDrawRadius);

        grid.draw(gc, viewport); // Redraw grid in lens interior (clipped at lens)
        graph.drawSelectedEdges(gc, selectedEdges); // Draw selected edges (clipped at lens)
        gc.restore(); // Restore original context state (no more clipping)
    }
};

Lens.prototype.transform = function (graph, viewport, position) {
    this.project(viewport);
    this.select(graph, position);
    this.bringneighbors(graph, position);
    this.fisheye(position);
};

Lens.prototype.select = function (graph, position) {

    this.nodesInLens = [];

    if (!this.active) return;

    var c = this.position.get();
    var r = this.radius.get()[0];
    var dx, dy;

    var n = graph.nodes.length;
    while (n--) {
        dx = position[n][0] - c[0];
        dy = position[n][1] - c[1];
        graph.nInLens[n] = (dx * dx + dy * dy < r * r);
        if (graph.nInLens[n]) this.nodesInLens.push(n);
    }
};

Lens.prototype.fisheye = function (position) {
    if (!(this.active && this.doFisheye)) return;

    var c = this.position.get();
    var r = this.radius.get()[0];
    var m = this.magnification.get()[0];
    var i;

    i = this.nodesInLens.length;
    while (i--) {
        this.fish2(position[this.nodesInLens[i]], c, r, m);
    }
};

Lens.prototype.fish = function (p, c, r, m) {
    var dx = p[0] - c[0];
    var dy = p[1] - c[1];
    var d2 = dx * dx + dy * dy;

    if (!d2) return;

    var dd = Math.sqrt(d2);
    var dq = dd / r;
    var dqn = (m + 1) * dq / (m * dq + 1);
    var dqc = dqn / dq;
    p[0] = dx * dqc + p[0] - dx;
    p[1] = dy * dqc + p[1] - dy;
};

Lens.prototype.fish2 = function (p, c, r, m) {
    /*
     * https://raw.githubusercontent.com/d3/d3-plugins/master/fisheye/fisheye.js
     * http://dl.acm.org/citation.cfm?id=142763
     */
    var k0 = Math.exp(m);
    k0 = k0 / (k0 - 1) * r;
    var k1 = m / r;

    var dx = p[0] - c[0];
    var dy = p[1] - c[1];
    var d2 = dx * dx + dy * dy;

    if (!d2) return;

    var dd = Math.sqrt(d2);
    var k = k0 * (1 - Math.exp(-dd * k1)) / dd * .75 + .25;
    p[0] = c[0] + dx * k;
    p[1] = c[1] + dy * k;
};

Lens.prototype.bringneighbors = function (graph, position) {
    if (!(this.active && this.doBringNeighbors)) return;
    var c = this.position.get();
    var r = this.radius.get()[0];

    // Determine for each lens node (1) the distance to the lens center, (2) the distance to the
    // farthest neighbor, and (3) a weight
    var distToCenter = [];
    var distToFarthestNeighbor = [];
    var attractorWeight = [];

    var affectedBy = {}; // Hash to store for each affected node the set of its attractors

    var dx, dy;
    var dist, distSq, maxDistSq;

    var n, m, i, j;

    i = this.nodesInLens.length;
    while (i--) {
        n = this.nodesInLens[i]; // The attractor node

        dx = position[n][0] - c[0]; // Compute distance to lens center
        dy = position[n][1] - c[1];
        dist = Math.sqrt(dx * dx + dy * dy);
        dist = Math.max(0, dist - this.centerTolerance); // Allow for some tolerance in terms of "center" of the lens
        distToCenter[n] = dist; // Store distance of attractor n to center of lens
        /* TODO: This weight has been determined experimentally, mathematical analysis is hard, pending question since 2005 */
        attractorWeight[n] = Math.pow(1 - distToCenter[n] / r, 64); // Play with the power

        maxDistSq = 0; // Initialize squared distance to farthest neighbor as 0

        j = graph.nNeighbors[n].length;
        while (j--) {
            m = graph.nNeighbors[n][j]; // A (potentially) affected node

            if (graph.nInLens[m]) continue; // Ignore this node, if it is already in the lens

            if (!affectedBy[m]) affectedBy[m] = {}; // Create a hash to collect attractors of affected node
            affectedBy[m][n] = true; // Add n as an attractor of m

            // Determine (squared) distance of attractor n and affected node m
            dx = position[n][0] - position[m][0];
            dy = position[n][1] - position[m][1];
            distSq = dx * dx + dy * dy;
            if (distSq > maxDistSq) maxDistSq = distSq; // Update max distance
        }
        distToFarthestNeighbor[n] = Math.sqrt(maxDistSq); // Store distance of attractor n to its farthest affected node
    }

    var affected = Object.keys(affectedBy); // Extract from hash the list of affected nodes
    i = affected.length;
    while (i--) {
        m = affected[i]; // The affected node

        var sumWeight = 0;
        var displacement = [0, 0];
        var focusNode;

        var attractors = Object.keys(affectedBy[m]); // Extract from hash the list of attractors that act on m

        // Summarize weights
        j = attractors.length;
        while (j--) {
            n = attractors[j]; // An attractor node

            if (distToCenter[n] == 0) { // Is node directly in the center of the lens (consider center tolerance)
                focusNode = n;
            }

            sumWeight += attractorWeight[n]; // Summarize weights of attractors related to affected node
        }

        // Summarize displacement of affected node
        j = attractors.length;
        while (j--) {
            n = attractors[j]; // An attractor node

            // Handle different cases for computing weight w for attractor n
            var weight = 1;
            if (focusNode) {
                if (n != focusNode) weight = 0; // If a node is at the center of the lens, all other weights are 0
            }
            else {
                if (attractors.length > 1) { // If multiple attractors take effect
                    weight = (sumWeight != 0) ? attractorWeight[n] / sumWeight : attractorWeight[n]; // Normalize weight
                }
            }

            var scale = r / distToFarthestNeighbor[n];
            scale = 1 - (scale + (1 - scale) * distToCenter[n] / r);
            dx = position[n][0] - position[m][0];
            dy = position[n][1] - position[m][1];
            dx *= scale * weight;
            dy *= scale * weight;

            displacement[0] += dx;
            displacement[1] += dy;
        }

        position[m][0] += displacement[0];
        position[m][1] += displacement[1];

        // Check if affected node's new position is inside lens
        dx = position[m][0] - c[0];
        dy = position[m][1] - c[1];
        graph.nInLens[m] = (dx * dx + dy * dy < r * r);
        if (graph.nInLens[m]) this.nodesInLens.push(m);
    }
};

Lens.prototype.bringneighbors2 = function (graph, position) {
    if (!(this.active && this.doBringNeighbors)) return;
    var c = this.position.get();
    var r = this.radius.get()[0];

    var neighbors = {}; // Use neighbors as hash to collect neighbors of lens nodes
    var min, max;
    var i, j, n, m;

    i = this.nodesInLens.length;
    while (i--) {
        n = this.nodesInLens[i];
        j = graph.nNeighbors[n].length;
        while (j--) {
            m = graph.nNeighbors[n][j];
            if (graph.nInLens[m]) continue; // Ignore neighbors that are already in the lens
            neighbors[m] = true; // Store neighbor in hash
        }
    }

    neighbors = Object.keys(neighbors); // Overwrite hash with list of actual neighbors

    // Determine bounding box of neighbors
    i = neighbors.length;
    while (i--) {
        n = neighbors[i];

        if (!min) min = [graph.nLayout[n][0], graph.nLayout[n][1]];
        if (!max) max = [graph.nLayout[n][0], graph.nLayout[n][1]];

        if (graph.nLayout[n][0] < min[0]) {
            min[0] = graph.nLayout[n][0];
        }
        else if (graph.nLayout[n][0] > max[0]) {
            max[0] = graph.nLayout[n][0];
        }

        if (graph.nLayout[n][1] < min[1]) {
            min[1] = graph.nLayout[n][1];
        }
        else if (graph.nLayout[n][1] > max[1]) {
            max[1] = graph.nLayout[n][1];
        }
    }

    // Treat special cases of zero-extent bounding box on x or y dimension
    if (min && min[0] == max[0]) {
        min[0] -= 1;
        max[0] += 1;
    }

    if (min && min[1] == max[1]) {
        min[1] -= 1;
        max[1] += 1;
    }

    // Scale and transform neighborhood to be centered on and within lens
    var innerQuadExtent = 2 * Math.sqrt(r * r * 0.5);
    i = neighbors.length;
    while (i--) {
        n = neighbors[i];
        position[n][0] = (position[n][0] - min[0]) / (max[0] - min[0]) * innerQuadExtent + c[0] - innerQuadExtent * 0.5;
        position[n][1] = (position[n][1] - min[1]) / (max[1] - min[1]) * innerQuadExtent + c[1] - innerQuadExtent * 0.5;

        // Updated node position is definitively inside the lens
        graph.nInLens[n] = true;
        this.nodesInLens.push(n);
    }
};
