# iGraph.js

## Project description 

This project is a demonstrator for the following interaction techniques for graph visualization:

* standard zooming and panning
* radar view
* graph lenses (fisheye lens, local-edge lens, bring-neighbors lens)

## Goals

Our goals are to:

* offer aesthetic graphics
* deliver self-explanatory and intuitive interaction
* provide smoothly animated feedback
* be better than the original implementation

## Project members

The following members contribute to this project:

* ab1117
* ct
* ct200
* sp437