function ForceDirectedLayout(graph) {

    var done = false;
    var nPosition = [];
    var nForce = [];
    var nFixed = [];

    var n, m, e;
    var p, q, d = [];
    var distance, distanceSq, force;

    n = graph.nodes.length;
    while (n--) {
        nPosition[n] = [Math.random() * 2 * Constants.LAYOUT_EXTENT - Constants.LAYOUT_EXTENT, Math.random() * 2 * Constants.LAYOUT_EXTENT - Constants.LAYOUT_EXTENT];
        nForce[n] = [0, 0];
        nFixed[n] = false;
    }

    this.fixNode = function (n, x, y) {
        graph.nLayout[n][0] = x;
        graph.nLayout[n][1] = y;
        nPosition[n][0] = x;
        nPosition[n][1] = y;
        nFixed[n] = true;

        done = false;
    };

    this.unfixNode = function (n) {
        nFixed[n] = false;

        done = false;
    };

    this.jiggle = function (scale) {
        n = graph.nodes.length;
        while (n--) {
            nPosition[n][0] += Math.random() * scale - scale / 2;
            nPosition[n][1] += Math.random() * scale - scale / 2;
        }

        done = false;
    };

    this.step = function (time) {
        if (done) return;

        // Gravitational forces toward (0, 0)
        n = graph.nodes.length;
        while (n--) {
            p = nPosition[n];

            // Vector from (0, 0) to p
            d[0] = p[0];
            d[1] = p[1];
            distanceSq = d[0] * d[0] + d[1] * d[1] + 0.01;  // Ensure distance != 0
            distance = Math.sqrt(distanceSq);
            force = Constants.LAYOUT_GRAVITY_FACTOR * distance; // * (graph.nDegree[n] + 1);

            nForce[n][0] = -force * d[0] / distance;
            nForce[n][1] = -force * d[1] / distance;
        }

        // Repelling forces, all nodes repel each other (Coulomb's Law)
        n = graph.nodes.length;
        while (n--) {
            p = nPosition[n];

            m = graph.nodes.length;
            while (m--) {
                if (n == m) continue; // No forces on identical nodes

                q = nPosition[m];

                // Vector from p to q
                d[0] = q[0] - p[0];
                d[1] = q[1] - p[1];
                distanceSq = d[0] * d[0] + d[1] * d[1] + 0.01;  // Ensure distance != 0
                distance = Math.sqrt(distanceSq);
                force = Constants.LAYOUT_REPULSION_FACTOR / distanceSq; // * (graph.nDegree[n] * graph.nDegree[m]);

                nForce[n][0] -= 0.5 * force * d[0] / distance; // Distribute force to nodes,
                nForce[n][1] -= 0.5 * force * d[1] / distance; // each node receives half of
                nForce[m][0] += 0.5 * force * d[0] / distance; // the force in opposite
                nForce[m][1] += 0.5 * force * d[1] / distance; // directions
            }
        }

        // Attracting forces, nodes connected by edges attract each other (Hooke's Law)
        e = graph.edges.length;
        while (e--) {
            n = graph.edges[e].src;
            m = graph.edges[e].dst;

            p = nPosition[n];
            q = nPosition[m];

            // Vector from p to q
            d[0] = q[0] - p[0];
            d[1] = q[1] - p[1];
            distanceSq = d[0] * d[0] + d[1] * d[1] + 0.01;  // Ensure distance != 0
            distance = Math.sqrt(distanceSq);
            force = Constants.LAYOUT_ATTRACTION_FACTOR * (Constants.LAYOUT_SPRING_LENGTH - distance);

            nForce[n][0] -= 0.5 * force * d[0] / distance; // Distribute force to nodes,
            nForce[n][1] -= 0.5 * force * d[1] / distance; // each node receives half of
            nForce[m][0] += 0.5 * force * d[0] / distance; // the force in opposite
            nForce[m][1] += 0.5 * force * d[1] / distance; // directions
        }

        // Update position
        var totalDisplacement = 0;

        n = graph.nodes.length;
        while (n--) {
            if (!nFixed[n]) {
                nPosition[n][0] += nForce[n][0];
                nPosition[n][1] += nForce[n][1];
                graph.nLayout[n][0] = nPosition[n][0];
                graph.nLayout[n][1] = nPosition[n][1];

                totalDisplacement += Math.sqrt(nForce[n][0] * nForce[n][0] + nForce[n][1] * nForce[n][1]);
            }
        }
        // console.log(totalDisplacement);

        // Stop if a good layout has been reached
        done = (totalDisplacement < Constants.LAYOUT_DISPLACEMENT_LIMIT);
        return !done;
    };
}