function Graph(nodes, edges) {

    var doColor = true;
    Object.defineProperty(this, "doColor",
        {
            get: function () {
                return doColor;
            },
            set: function (value) {
                doColor = value;
            }
        }
    );

    var doSize = true;
    Object.defineProperty(this, "doSize",
        {
            get: function () {
                return doSize;
            },
            set: function (value) {
                doSize = value;
            }
        }
    );

    var doWidth = true;
    Object.defineProperty(this, "doWidth",
        {
            get: function () {
                return doWidth;
            },
            set: function (value) {
                doWidth = value;
            }
        }
    );

    this.nodes = nodes;

    this.nEdges = [];
    this.nNeighbors = [];
    this.nDegree = [];
    this.nLayout = [];
    this.nInLens = [];
    this.nInRadar = [];
    this.nPosition = [];
    this.nDrawPosition = [];
    this.nMappingValue = [];
    this.nRadius = [];
    this.nColor = [];

    this.edges = edges;
    this.eInLens = [];
    this.eMappingValue = [];
    this.eWidth = [];

    this.bounds = {x: 0, y: 0, w: 1, h: 1};
    this.picked = false;

    var n, e, maxDegree = 0, maxValue = 0;

    n = nodes.length;
    while (n--) {
        this.nodes[n].id = n;
        this.nEdges[n] = [];
        this.nNeighbors[n] = [];
        this.nDegree[n] = 0;
        this.nLayout[n] = [0, 0];
        this.nInLens[n] = false;
        this.nInRadar[n] = false;
        this.nPosition[n] = new Animatable([0, 0]);
        this.nDrawPosition[n] = [];
        this.nRadius[n] = 0;
        this.nColor[n] = 'rgb(0, 0, 0)';
    }

    e = edges.length;
    while (e--) {
        this.edges[e].id = e;
        if (this.edges[e].val > maxValue) maxValue = this.edges[e].val;
        this.eInLens[e] = false;

        n = this.edges[e].src; // Index of the source node
        this.nEdges[n].push(e);  // Store incident edge at source node
        this.nNeighbors[n].push(this.edges[e].dst);  // Store source node as neighbor of destination node
        this.nDegree[n]++; // Increment degree of source node
        if (this.nDegree[n] > maxDegree) maxDegree = this.nDegree[n];

        n = this.edges[e].dst; // Index of the destination node
        this.nEdges[n].push(e); // Store incident edge at destination node
        this.nNeighbors[n].push(this.edges[e].src); // Store destination node as neighbor of source node
        this.nDegree[n]++; // Increment degree of destination node
        if (this.nDegree[n] > maxDegree) maxDegree = this.nDegree[n];
    }

    n = nodes.length;
    while (n--) {
        this.nMappingValue[n] = this.nDegree[n] / maxDegree;
    }

    e = edges.length;
    while (e--) {
        this.eMappingValue[e] = this.edges[e].val / maxValue;
    }

    this.encodeSizeAndColor = function() {
        var minArea = Constants.MIN_RADIUS * Constants.MIN_RADIUS * Math.PI / 4;
        var maxArea = Constants.MAX_RADIUS * Constants.MAX_RADIUS * Math.PI / 4;

        n = nodes.length;
        while (n--) {
            var t = this.nMappingValue[n];
            this.nColor[n] = (doColor) ? chroma.scale("YlGn").mode("lab")(t).hex() : 'rgb(190, 190, 190)';

            var area = minArea * (1 - t) + maxArea * t;
            this.nRadius[n] = (doSize) ? Math.sqrt(area / Math.PI) : Constants.FIX_RADIUS;
        }

        e = edges.length;
        while (e--) {
            var t = this.eMappingValue[e];
            this.eWidth[e] = (doWidth) ? Constants.MIN_WIDTH * (1 - t) + Constants.MAX_WIDTH * t : Constants.FIX_WIDTH;
        }

    };

    this.encodeSizeAndColor();

    this.layout = new ForceDirectedLayout(this);

    /*
     For faster rendering of nodes, we could create images (i.e., canvases) and render these images,
     rather than filling and stroking paths. For each possible node degree, we create a correspondingly
     colored and scaled image and store it in array img. Later when rendering a node, we simply select
     an image according to node's degree and render that image at the node's position. This is faster
     than rasterizing the node circle over and over again.

     Uncomment here and in draw method to use image-based node drawing

     this.img = [];

     var d = max + 1;
     while (d--) {
     var t = d / max;
     var colIndex = Math.round(t * (Constants.NODE_COLORS.length - 1));
     var dColor = Constants.NODE_COLORS[colIndex];

     var area = minArea * (1 - t) + maxArea * t;
     var dRadius = Math.sqrt(area / Math.PI);

     this.img[d] = document.createElement("canvas");
     var igc = this.img[d].getContext("2d");
     this.img[d].height = dRadius * 2 + 10;
     this.img[d].width = dRadius * 2 + 10;
     igc.strokeStyle = Constants.COLOR_LINE_REGULAR;
     igc.fillStyle = dColor;
     igc.shadowColor = 'rgb(96, 96, 96)';
     igc.shadowOffsetX = 2;
     igc.shadowOffsetY = 2;
     igc.shadowBlur = 4;
     igc.beginPath();
     igc.arc(dRadius + 1, dRadius + 1, dRadius, 0, 2 * Math.PI);
     igc.fill();
     igc.shadowColor = '';
     igc.shadowOffsetX = 0;
     igc.shadowOffsetY = 0;
     igc.shadowBlur = 0;
     igc.stroke();
     }

     */
}

Graph.prototype.pick = function (evt) {
    var dx;
    var dy;

    this.picked = false;

    var n = this.nodes.length;
    while (n-- && !this.picked) {
        dx = this.nDrawPosition[n][0] - evt.screenCoords[0];
        dy = this.nDrawPosition[n][1] - evt.screenCoords[1];
        // Use 1 + n as pick id to be able to pick node with index 0 (which would be interpreted as "false" otherwise)
        this.picked = (dx * dx + dy * dy < this.nRadius[n] * this.nRadius[n]) ? 1 + n : false;
    }

    return this.picked;
};

Graph.prototype.onmousedown = function (evt) {
    if (!this.dragging) {
        if (evt.button == 0) {
            this.dragging = true;
        }
    }
    return true; // Event consumed
};

Graph.prototype.onmouseup = function (evt) {
    if (this.dragging) {
        if (evt.button == 0) {
            if (!evt.shiftKey) this.layout.unfixNode(this.picked - 1);
            delete this.dragging;
        }
    }
    return true; // Event consumed
};

Graph.prototype.onmousemove = function (evt) {
    if (this.dragging) {
        this.layout.fixNode(this.picked - 1, evt.worldCoords[0], evt.worldCoords[1]);
    }
    return true; // Event consumed
};

Graph.prototype.update = function (time) {
    var needUpdate;

    needUpdate = this.layout.step(time);

    var n = this.nodes.length;
    while (n--) {
        needUpdate = this.nPosition[n].update(time) || needUpdate;
    }
    return needUpdate;
};

Graph.prototype.project = function (viewport) {
    var n = this.nodes.length;
    while (n--) {
        this.nDrawPosition[n] = viewport.project(this.nPosition[n].get());
    }
};

Graph.prototype.drawEdges = function (gc) {
    gc.strokeStyle = Constants.COLOR_LINE_REGULAR;
    var p;
    var e = this.edges.length;
    while (e--) {
        gc.beginPath();
        p = this.nDrawPosition[this.edges[e].src];
        gc.moveTo(p[0], p[1]);
        p = this.nDrawPosition[this.edges[e].dst];
        gc.lineTo(p[0], p[1]);
        gc.lineWidth = this.eWidth[e];
        gc.stroke();
    }
};

Graph.prototype.drawSelectedEdges = function (gc, selectedEdges) {
    var e, p;
    var s = selectedEdges.length;
    while (s--) {
        e = selectedEdges[s];
        gc.beginPath();
        gc.strokeStyle = this.eInLens[e] ? Constants.COLOR_LINE_HIGHLIGHT : Constants.COLOR_LINE_REGULAR;
        gc.lineWidth = this.eWidth[e];
        p = this.nDrawPosition[this.edges[e].src];
        gc.moveTo(p[0], p[1]);
        p = this.nDrawPosition[this.edges[e].dst];
        gc.lineTo(p[0], p[1]);
        gc.stroke();
    }
};

Graph.prototype.drawNodes = function (gc, lensactive, radaractive) {
    gc.lineWidth = Constants.LINE_WIDTH_NODE;
    var p;
    var n = this.nodes.length;
    while (n--) {
        p = this.nDrawPosition[n];
        gc.beginPath();
        gc.strokeStyle = (lensactive && this.nInLens[n]) || (radaractive && this.nInRadar[n]) ? Constants.COLOR_LINE_HIGHLIGHT : Constants.COLOR_LINE_REGULAR;
        gc.fillStyle = this.nColor[n];
        gc.arc(p[0], p[1], this.nRadius[n], 0, 2 * Math.PI);
        gc.fill();
        gc.stroke();

        // Uncomment below and comment above for image-based node rendering
        // var i = this.img[this.nDegree[n]];
        // gc.drawImage(i, p[0] - (i.width - 10) / 2 - 1, p[1] - (i.height - 10) / 2 - 1);
    }
};

Graph.prototype.computeBounds = function () {
    var p = this.nLayout[0];
    var bx = p[0];
    var by = p[1];
    var bw = p[0];
    var bh = p[1];

    var n = this.nodes.length;
    while (n--) {
        p = this.nLayout[n];
        var minx = p[0];
        var miny = p[1];
        var maxx = p[0];
        var maxy = p[1];
        if (minx < bx) bx = minx;
        if (miny < by) by = miny;
        if (maxx > bw) bw = maxx;
        if (maxy > bh) bh = maxy;
    }


    bw = bw - bx; // Convert minx and maxx to width
    bh = bh - by; // Convert miny and maxy to height
    if (bw == 0) bw = 1;
    if (bh == 0) bh = 1;

    this.bounds = {x: bx, y: by, w: bw, h: bh};
};

function JSONGraph(json) {
    Graph.call(this, json.nodes, json.edges);
}

JSONGraph.prototype = Object.create(Graph.prototype);

function RandomGraph(numNodes, numEdges) {

    // Create nodes
    var nodes = [];
    for (var i = 0; i < numNodes; i++) {
        nodes.push({id: i, x: Math.random() * 100, y: Math.random() * 100});
    }

    // Create edges
    var edges = [];
    for (i = 0; i < numEdges; i++) {
        var srcIdx = Math.round(Math.random() * (numNodes - 1));
        var dstIdx = srcIdx;
        while (dstIdx === srcIdx)
            dstIdx = Math.round(Math.random() * (numNodes - 1));

        edges.push({src: srcIdx, dst: dstIdx, val: Math.random()});
    }

    Graph.call(this, nodes, edges);
}

RandomGraph.prototype = Object.create(Graph.prototype);

