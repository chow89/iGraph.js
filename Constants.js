var Constants =
{
    ZOOM_WHEEL_FACTOR: 0.7,
    ZOOM_KEY_FACTOR: 0.7,
    MOVE_KEY_FACTOR: 0.25,

    MIN_RADIUS: 5,
    MAX_RADIUS: 50,
    FIX_RADIUS: 7,

    MIN_WIDTH: 0.2,
    MAX_WIDTH: 7,
    FIX_WIDTH: 1,

    LINE_WIDTH_NODE: 2,
    LINE_WIDTH_EDGE: 1,
    LINE_WIDTH_GRID: 1,
    LINE_WIDTH_LENS: 1,
    LINE_WIDTH_RADAR: 1,

    COLOR_LINE_REGULAR: 'rgb(128, 128, 128)',
    COLOR_LINE_HIGHLIGHT: 'rgb(32,   32,  32)',
    COLOR_RADAR_WHEEL: 'rgb(200, 200, 200)',
    COLOR_RADAR_ARC: 'rgb(255, 255, 255)',
    COLOR_RADAR_ARC_HANDLE: 'rgb(200, 200, 200)',

    KEY_A: 'a'.charCodeAt(),
    KEY_B: 'b'.charCodeAt(),
    KEY_C: 'c'.charCodeAt(),
    KEY_D: 'd'.charCodeAt(),
    KEY_E: 'e'.charCodeAt(),
    KEY_F: 'f'.charCodeAt(),
    KEY_G: 'g'.charCodeAt(),
    KEY_H: 'h'.charCodeAt(),
    KEY_L: 'l'.charCodeAt(),
    KEY_O: 'o'.charCodeAt(),
    KEY_P: 'p'.charCodeAt(),
    KEY_R: 'r'.charCodeAt(),
    KEY_S: 's'.charCodeAt(),
    KEY_Q: 'q'.charCodeAt(),
    KEY_W: 'w'.charCodeAt(),
    KEY_U: 'u'.charCodeAt(),
    KEY_PLUS: '+'.charCodeAt(),
    KEY_MINUS: '-'.charCodeAt(),
    KEY_SPACE: ' '.charCodeAt(),
    KEY_ONE: '1'.charCodeAt(),
    KEY_TWO: '2'.charCodeAt(),
    KEY_THREE: '3'.charCodeAt(),

    LAYOUT_EXTENT: 10,

    LAYOUT_ATTRACTION_FACTOR: 0.1,
    LAYOUT_GRAVITY_FACTOR: 0.01,
    LAYOUT_REPULSION_FACTOR: 0.1,
    LAYOUT_SPRING_LENGTH: 1,

    LAYOUT_DISPLACEMENT_LIMIT: 1,

    NUMBER_NODES: 50,
    NUMBER_EDGES: 100,

    RADAR_RADIUS: 40,
    RADAR_HANDLE_SCALE: 1.2,
    RADAR_RANGE: Math.PI / 2,
    FISHEYE_MAGNIFICATION: 3,
    BRING_NEIGHBORS_CENTER_TOLERANCE: 10, // 10 pixels = 5 pixel radius
};
